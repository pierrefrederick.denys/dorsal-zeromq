#Test librairie liblttngzmq

##Installation de libzmq instrumentée

Installation de la librairie libzmq via les sources :

> cd /usr/local
>
> git clone https://gitlab.com/pierrefrederick.denys/dorsal-zeromq.git 
>
> cp dorsal-zeromq/test-libzmq/liblttngzmq.so /usr/local/lib/ 
>
> ldconfig
>
> cd /usr/local/dorsal-zeromq/libzmq-inst/
>
> ./autogen.sh && ./configure LIBS="-ldl -llttng-ust -llttngzmq" && make && make install && ldconfig


##Utilisation de la librairie instrumentée

Exemple de compilation avec la librairie instrumentée
>g++ exemple.cpp -o exemple -lzmq

> lttng-sessiond --daemonize
>
>lttng create
>
>lttng enable-event --userspace lttng_tracing:zmq_msg_send_event,lttng_tracing:zmq_socket_connect_event,lttng_tracing:zmq_msg_recv_event,lttng_tracing:zmq_socket_disconnect_event
>
>lttng start
> 
>./exemple  
>
>lttng stop
>
>lttng view