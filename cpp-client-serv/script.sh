lttng-sessiond --daemonize
lttng create
lttng enable-event --userspace lttng_tracing:zmq_msg_send_event,lttng_tracing:zmq_socket_connect_event,lttng_tracing:zmq_msg_recv_event,lttng_tracing:zmq_socket_disconnect_event
lttng start
./server &
./client
lttng stop
lttng view