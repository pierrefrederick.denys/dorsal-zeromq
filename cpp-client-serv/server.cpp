//  Server for zmq instrumented version tests in C++
//  Binds REP socket to tcp://*:5555
//  Expects a message from the client, replies with "Message received"
//
#include "cppzmq/zmq.hpp"
#include <string>
#include <iostream>
#ifndef _WIN32
#include <unistd.h>
#else
#include <windows.h>

#define sleep(n)    Sleep(n)
#endif

int main () {
    //  Prepare our context and socket
    printf ("%s \n", "prepare socket");
    zmq::context_t context (1);
    zmq::socket_t socket (context, ZMQ_REP);
    socket.bind ("tcp://*:5555");

    while (true) {
        zmq::message_t request;

        //  Wait for next request from client
        socket.recv (&request);
        std::cout << "Get the message from the client" << std::endl;
        printf ("%s \n", "A string");

        //  Do some 'work'
        sleep(1);

        //  Send reply back to client
        zmq::message_t reply (20);
        memcpy (reply.data (), "Message received", 20);
        socket.send (reply);
    }
    return 0;
}