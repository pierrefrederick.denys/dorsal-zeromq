//  Server for zmq instrumented version tests in C++
//  Connects REQ socket to tcp://localhost:5555
//  Sends "Hello I am a client" to server, expects "Message received" back
//
#include "cppzmq/zmq.hpp"
#include <string>
#include <iostream>
#include <time.h>

int main ()
{
    //  Prepare our context and socket
    zmq::context_t context (1);
    zmq::socket_t socket (context, ZMQ_REQ);

    std::cout << "Connecting to server ^` " << std::endl;
    socket.connect ("tcp://localhost:5555");

    //  Do 10 requests, waiting each time for a response
    for (int request_nbr = 0; request_nbr != 10; request_nbr++) {
        zmq::message_t request (19);
        memcpy (request.data (), "Hello I am a client", 19);
        std::cout << "Sending message to server" << request_nbr << " ^` " << std::endl;
        socket.send (request);

        //  Get the reply.
        zmq::message_t reply;
        socket.recv (&reply);
        std::cout << "Received acknowledgment" << request_nbr << std::endl;
    }
    return 0;
}





