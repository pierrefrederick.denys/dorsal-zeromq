# Dorsal ZeroMQ

##Introduction

Le but de ce projet est d'instrumenter la bibliothèque lizmq, afin d'y ajouter des points de trace. 

Les points de trace sont ajoutés aux fonctions zmq_msg_send zmq_msg_recv, zmq_connect et zmq_disconnect

##Points de trace

###Trace provider

Le trace provider est situé dans le fichier lttng-tracing.c 

###Trace points


##Programme de test 

Le code du programme de test se trouve dans le dossier cpp-client-serv, et est automatiquement compilé par le fichier dockerfile. 
Le script sh permet de lancer le programme et une session de traçage lttng. La sortie du script affiche les traces générées.

> docker build --no-cache . -t zmq-inst-v1.1
>
> docker run -it zmq-inst-v1.1:latest
>
> Dans la console lancer le script : root@<container-id>:/usr/local/dorsal-zeromq/cpp-client-serv# ./script.sh
>
Vous devriez avoir la sortie du programme client, suivi de l'affichage des traces avec lttng view.