#undef TRACEPOINT_PROVIDER
#define TRACEPOINT_PROVIDER lttng_tracing

#undef TRACEPOINT_INCLUDE
#define TRACEPOINT_INCLUDE "./lttng-tracing.h"

#if !defined(_LTTNG_TRACING_H) || defined(TRACEPOINT_HEADER_MULTI_READ)
#define _LTTNG_TRACING_H

#include <lttng/tracepoint.h>


TRACEPOINT_EVENT(
        lttng_tracing,
        zmq_socket_connect_event,
        TP_ARGS(
        char*, socket_name
),
        TP_FIELDS(
        ctf_string(socketName, socket_name)
)
)

TRACEPOINT_EVENT(
        lttng_tracing,
        zmq_socket_disconnect_event,
        TP_ARGS(
        char*, socket_name
),
        TP_FIELDS(
        ctf_string(socketName, socket_name)
)
)

TRACEPOINT_EVENT(
        lttng_tracing,
        zmq_msg_send_event,
        TP_ARGS(
        int, uid,
        char*, msg,
        char*, address,
        int, hwm
),
        TP_FIELDS(
        ctf_string(dest_address, address)
        ctf_string(message, msg)
        ctf_integer(int, uid, uid)
        ctf_integer(int, highWaterMark, hwm)
)
)

TRACEPOINT_EVENT(
        lttng_tracing,
        zmq_msg_recv_event,
        TP_ARGS(
        int, uid,
        char*, msg,
        char*, address,
        int, hwm
),
        TP_FIELDS(
        ctf_string(sender_address, address)
ctf_string(message, msg)
ctf_integer(int, uid, uid)
ctf_integer(int, highWaterMark, hwm)
)
)


#endif /* _HELLO_TP_H */

#include <lttng/tracepoint-event.h>