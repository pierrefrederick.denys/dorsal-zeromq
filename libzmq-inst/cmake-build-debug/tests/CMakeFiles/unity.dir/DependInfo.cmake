# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/pierre/Projets/POLYMTL/zeromq-ciena/libzmq/external/unity/unity.c" "/home/pierre/Projets/POLYMTL/zeromq-ciena/libzmq/cmake-build-debug/tests/CMakeFiles/unity.dir/__/external/unity/unity.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "UNITY_EXCLUDE_FLOAT"
  "UNITY_USE_COMMAND_LINE_ARGS"
  "ZMQ_BUILD_DRAFT_API"
  "ZMQ_CUSTOM_PLATFORM_HPP"
  "ZMQ_HAVE_TIPC"
  "ZMQ_USE_RADIX_TREE"
  "_GNU_SOURCE"
  "_REENTRANT"
  "_THREAD_SAFE"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../tests/../external/unity"
  "../../include"
  "."
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
