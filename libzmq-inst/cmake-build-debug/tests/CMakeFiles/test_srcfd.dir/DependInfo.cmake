# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/pierre/Projets/POLYMTL/zeromq-ciena/libzmq/tests/test_srcfd.cpp" "/home/pierre/Projets/POLYMTL/zeromq-ciena/libzmq/cmake-build-debug/tests/CMakeFiles/test_srcfd.dir/test_srcfd.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "UNITY_EXCLUDE_FLOAT"
  "UNITY_USE_COMMAND_LINE_ARGS"
  "ZMQ_BUILD_DRAFT_API"
  "ZMQ_CUSTOM_PLATFORM_HPP"
  "ZMQ_HAVE_TIPC"
  "ZMQ_USE_RADIX_TREE"
  "_GNU_SOURCE"
  "_REENTRANT"
  "_THREAD_SAFE"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../../include"
  "."
  "../include"
  "../tests/../external/unity"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/pierre/Projets/POLYMTL/zeromq-ciena/libzmq/cmake-build-debug/tests/CMakeFiles/testutil.dir/DependInfo.cmake"
  "/home/pierre/Projets/POLYMTL/zeromq-ciena/libzmq/cmake-build-debug/CMakeFiles/libzmq.dir/DependInfo.cmake"
  "/home/pierre/Projets/POLYMTL/zeromq-ciena/libzmq/cmake-build-debug/tests/CMakeFiles/unity.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
