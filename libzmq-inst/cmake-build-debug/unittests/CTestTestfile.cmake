# CMake generated Testfile for 
# Source directory: /home/pierre/Projets/POLYMTL/zeromq-ciena/libzmq/unittests
# Build directory: /home/pierre/Projets/POLYMTL/zeromq-ciena/libzmq/cmake-build-debug/unittests
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(unittest_ypipe "/home/pierre/Projets/POLYMTL/zeromq-ciena/libzmq/cmake-build-debug/bin/unittest_ypipe")
set_tests_properties(unittest_ypipe PROPERTIES  TIMEOUT "10" _BACKTRACE_TRIPLES "/home/pierre/Projets/POLYMTL/zeromq-ciena/libzmq/unittests/CMakeLists.txt;52;add_test;/home/pierre/Projets/POLYMTL/zeromq-ciena/libzmq/unittests/CMakeLists.txt;0;")
add_test(unittest_poller "/home/pierre/Projets/POLYMTL/zeromq-ciena/libzmq/cmake-build-debug/bin/unittest_poller")
set_tests_properties(unittest_poller PROPERTIES  TIMEOUT "10" _BACKTRACE_TRIPLES "/home/pierre/Projets/POLYMTL/zeromq-ciena/libzmq/unittests/CMakeLists.txt;52;add_test;/home/pierre/Projets/POLYMTL/zeromq-ciena/libzmq/unittests/CMakeLists.txt;0;")
add_test(unittest_mtrie "/home/pierre/Projets/POLYMTL/zeromq-ciena/libzmq/cmake-build-debug/bin/unittest_mtrie")
set_tests_properties(unittest_mtrie PROPERTIES  TIMEOUT "10" _BACKTRACE_TRIPLES "/home/pierre/Projets/POLYMTL/zeromq-ciena/libzmq/unittests/CMakeLists.txt;52;add_test;/home/pierre/Projets/POLYMTL/zeromq-ciena/libzmq/unittests/CMakeLists.txt;0;")
add_test(unittest_ip_resolver "/home/pierre/Projets/POLYMTL/zeromq-ciena/libzmq/cmake-build-debug/bin/unittest_ip_resolver")
set_tests_properties(unittest_ip_resolver PROPERTIES  TIMEOUT "10" _BACKTRACE_TRIPLES "/home/pierre/Projets/POLYMTL/zeromq-ciena/libzmq/unittests/CMakeLists.txt;52;add_test;/home/pierre/Projets/POLYMTL/zeromq-ciena/libzmq/unittests/CMakeLists.txt;0;")
add_test(unittest_udp_address "/home/pierre/Projets/POLYMTL/zeromq-ciena/libzmq/cmake-build-debug/bin/unittest_udp_address")
set_tests_properties(unittest_udp_address PROPERTIES  TIMEOUT "10" _BACKTRACE_TRIPLES "/home/pierre/Projets/POLYMTL/zeromq-ciena/libzmq/unittests/CMakeLists.txt;52;add_test;/home/pierre/Projets/POLYMTL/zeromq-ciena/libzmq/unittests/CMakeLists.txt;0;")
add_test(unittest_radix_tree "/home/pierre/Projets/POLYMTL/zeromq-ciena/libzmq/cmake-build-debug/bin/unittest_radix_tree")
set_tests_properties(unittest_radix_tree PROPERTIES  TIMEOUT "10" _BACKTRACE_TRIPLES "/home/pierre/Projets/POLYMTL/zeromq-ciena/libzmq/unittests/CMakeLists.txt;52;add_test;/home/pierre/Projets/POLYMTL/zeromq-ciena/libzmq/unittests/CMakeLists.txt;0;")
