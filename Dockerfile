
FROM ubuntu:18.04
MAINTAINER PierreFrederick DENYS

RUN apt-get update &&  apt-get install -y \
software-properties-common \
python \
nodejs \
git \
build-essential \
libtool \
autoconf \
automake \
pkg-config \
unzip \
libkrb5-dev \
cmake && apt-add-repository ppa:lttng/stable-2.10 && apt-get update && \
apt-get install -y \
lttng-modules-dkms \
lttng-tools \
liblttng-ust-dev

RUN apt-get install -y gcc

WORKDIR /tmp


RUN git clone git://github.com/jedisct1/libsodium.git

WORKDIR /tmp/libsodium

RUN ./autogen.sh && ./configure && make check && make install && ldconfig 

WORKDIR /usr/local

RUN git clone https://gitlab.com/pierrefrederick.denys/dorsal-zeromq.git

WORKDIR /usr/local/dorsal-zeromq/libzmq-inst/src/ 

RUN gcc -I. -fpic -c lttng-tracing.c && gcc -shared -o liblttngzmq.so lttng-tracing.o -llttng-ust -ldl && ldconfig

RUN cp liblttngzmq.so /usr/local/lib/ 

ENV LD_LIBRARY_PATH /usr/local/lib/ 

RUN ldconfig

WORKDIR /usr/local/dorsal-zeromq/libzmq-inst/

RUN ./autogen.sh && ./configure LIBS="-ldl -llttng-ust -llttngzmq" && make && make install && ldconfig

WORKDIR /usr/local/dorsal-zeromq/cpp-client-serv/

RUN git clone https://github.com/zeromq/cppzmq.git

WORKDIR /usr/local/dorsal-zeromq/cpp-client-serv/

RUN g++ server.cpp -o server -lzmq

RUN g++ client.cpp -o client -lzmq

RUN chmod +x script.sh